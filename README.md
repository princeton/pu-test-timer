# README #

This is a Blackboard plug-in that allows test builders to install a JavaScript timer on the test page.


### Forcing Submission of Blackboard Assessments at Time-up ###
  
Install the plug-in.

Create a new Test. Give it a Name and Description.
  
In the "Instructions" section, make sure you select "HTML" below the text box.  Also make sure the Visual Text Box Editor is OFF. Insert a script tag, with its src attribute set to the path to the

    counter_endtime.js

script supplied by this plug-in.
  	
Click Submit. Finish creating the test.
  
Add the Test to your course content. In the "Test Options" section, under Test Availability, just make sure you set the timer.
  
### The Ten-Second Issue ###
  
This script is written to cause the test to submit 10 seconds before the actual time is up, to allow time for the submission process -- though you 
will encounter some cases where it takes longer than 10 seconds to submit, 
resulting in a "Needs Grading" message. I would suggest have 
students use Durham University's Browser Tester building block or something 
similar, as it has an Alert Test to make sure the 1 minute warning will appear. 
Sometimes a test will go minutes over the time limit because the student 
did not see or click away the alert -- but they cannot work on the test 
during this time either.

### Message to Students ###

We suggest you include this message to students in the instructions:

If you do not submit the test at least 10 seconds before the time is up, 
the test will submit automatically. So if the test has a 60 minute time limit, 
you actually only have 59 minutes, 50 seconds to complete the test, 
as the time it takes to complete the submission process, 
which can take up to 10 seconds, is figured in the time.

The test will not submit automatically until student clicks away the 1-minute warning pop-up, 
though the timer keeps ticking -- so if the student waits 2 minutes 
to click the pop-up, the test will show that the student went 1 minute overtime. 
However, the student cannot continue working on the test while the pop-up is displayed
-- nor after it has been clicked away, if the time limit has elapsed.

### Separating functionality ###

If you prefer to have only the timed test portion or the no-print/no-copy portion of the scripts, they are provided here as separate scripts.

For timed tests only, follow the instructions above, but use:

    counter_endtime-timer-only.js

For copy and print restrictions use this sequence:

    no-copy.js


### Who do I talk to? ###

* blackboard@princeton.edu