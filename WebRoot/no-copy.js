/*
 * No-print no-copy original code thanks to:
 *  david carter-tod (formerly vis. http://www.wcc.vccs.edu/dtod/mylicense.txt)
 *  (last known as http://david.carter-tod.com/)
 */

function deselect(){
  if (document.selection) {
    //document.selection.empty();
    document.selection.clear();
    Copied=document.body.createTextRange();
    Copied.findText("Please do not copy or print this page");
    Copied.execCommand("Copy");
  } else if (window.getSelection) {
    /*
     * Extra checks to fix Chrome and Safari 5 handling of removeAllRanges()
     * Contributed by Damian Sweeney, dsweeney@unimelb.edu.au
    **/
    if (window.getSelection().rangeCount > 0) {
      if (window.getSelection().getRangeAt(0) != '') {
        window.getSelection().removeAllRanges();
      }
    }
  }
}

if (!(location.href.indexOf("/bin/common/grade_asmt.pl")>1 && location.href.indexOf("&subroutine=analysis")>1)) {
  timerID=setTimeout('setInterval("deselect()", 5)',2000);
  document.writeln("<span style=\"display:none;\">Please do not copy or print this page.</span>");
  document.writeln("<style type=\"text/css\" media=\"print\">BODY{display:none;}</style>");
}

/*
 * Disable Right Click Menu code thanks to:
 * Peter Majeed : http://stackoverflow.com/users/684271/peter-majeed
 */
if (document.addEventListener) {
	document.addEventListener('contextmenu', function(e) {
		alert("You cannot use your right mouse button during the test.");
		e.preventDefault();
	}, false);
} else {
	document.attachEvent('oncontextmenu', function() {
		alert("You cannot use the right mouse button during the test");
		window.event.returnValue = false;
		});
}
