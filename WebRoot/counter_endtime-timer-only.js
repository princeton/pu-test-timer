/*
 * KP 3/30/2010: PU modifications ported/added to Bb9 counter.js file:
 *    - submit test when time is up.
 *
 * All local mods marked with "PU" comment.
 *
 * Submitting test when time is up: KDC 1/25/2006; KP 6/6/2007
 */

/*** PU: set-up for submitting test when time is up ***/

document.writeln("<input type='image' name='KPsubmit' border='0' alt='' src='/images/spacer.gif' onclick='javascript:save_and_submit.value=\"true\"; return submitIt(submitAssessment);'>");

var endTime;

function submitIt(msg)
{
  if ( checkAlreadySubmitted('Automatic submission is being processed. Wait for confirmation.') )
  {
    window.top.document.title = "Blackboard Learn";
    return true;
  }
  return false;
}

/*** end set-up for submitting test ***/

useBeep = true;
window.OnError = null;

var statusMsg;
var timerId = null;
var timerRunning = false;

Seconds = 0;
Minutes = 0;
Hours   = 0;
SecondsBeforeWarn = 0;

var startTime;
var warningTime;
var warningPosted = 0;

function showtime()
{
  currentTime = new Date().getTime();

  elapsedMS = currentTime - startTime;

  Hours   = Math.floor( elapsedMS / 3600000 );
  Minutes = Math.floor( ( elapsedMS - (Hours*3600000) ) / 60000 );
  Seconds = Math.floor( ( elapsedMS - (Hours*3600000) - (Minutes*60000) ) / 1000 );
  
  if ( (currentTime > warningTime) && (currentTime < (warningTime+2000)) && (! warningPosted) )
  {
    showTimerAlert(warningMin, warningSec);
    warningPosted = 1;
  }

  /*** PU: for submitting test when time is up ***/
  if ( currentTime >= endTime )
  {
    stopClock();
    (document.getElementsByName("KPsubmit").item(0)).click();
    return false;
  }
  /*** end for submitting test when time is up ***/

  hourTitle="";
  minuteTitle="";
  secondTitle="";
  if (Hours == 1)
    hourTitle=ASSESSMENT_RESOURCES.getFormattedString("elapsed_time_title.hour", new Array(''+Hours));
  else if (Hours>1)
    hourTitle=ASSESSMENT_RESOURCES.getFormattedString("elapsed_time_title.hours", new Array(''+Hours));
  
  if (Minutes == 1)
    minuteTitle=ASSESSMENT_RESOURCES.getFormattedString("elapsed_time_title.minute", new Array(''+Minutes));
  else if (Minutes>1)
    minuteTitle=ASSESSMENT_RESOURCES.getFormattedString("elapsed_time_title.minutes", new Array(''+Minutes));

  if (Seconds == 1)
    secondTitle=ASSESSMENT_RESOURCES.getFormattedString("elapsed_time_title.second", new Array(''+Seconds));
  else
    secondTitle=ASSESSMENT_RESOURCES.getFormattedString("elapsed_time_title.seconds", new Array(''+Seconds));
  
  window.top.document.title = ASSESSMENT_RESOURCES.getFormattedString("elapsed_time_title") +" " +hourTitle+" " +minuteTitle+" "+secondTitle;
    
  timerId = setTimeout("showtime()", 500);
  timerRunning = true;
 
  document.getElementById("cl").innerHTML = ASSESSMENT_RESOURCES.getFormattedString("elapsed_time_title") +" "+hourTitle+" " +minuteTitle+" " +secondTitle;
 }

function resetBrowserTitle(title) 
{
    window.top.document.title =title;
} 

function stopClock()
{
  if (timerRunning)
  {
    clearTimeout(timerId);
  }
  timerRunning = false;
}

function startClock(elapsedSec, timeLimitSec, timeLeftSecWarning)
{
  stopClock();

  startTime = new Date().getTime() - (elapsedSec * 1000);
  warningTime = startTime + (timeLimitSec - timeLeftSecWarning) * 1000;
  warningMin = Math.floor(timeLeftSecWarning / 60);
  warningSec = Math.floor( timeLeftSecWarning - (warningMin * 60) );

  /*** PU: for submitting test when time is up ***/

  // Submit 10 seconds early to overcome time it takes to submit
  // (internal clock doesn't stop).
  endTime = startTime + timeLimitSec * 1000 - 10000;

  /*** end for submitting test when time is up ***/

  showtime();
}

function showConfirm(message)
{
  return ( confirm(Message) ? true : false );
}

/**
 * This would be simpler if we could ignore seconds when there is at least a minute left
**/
function showTimerAlert(warningMin, warningSec)
{
  if (warningMin < 0) { warningMin = 0; }
  if (warningSec < 0) { warningSec = 0; }

  var messageKey;

  if (warningMin > 1 && warningSec > 1)
  {
    messageKey = "alert.time_left.minutes_seconds";
  }
  else if (warningMin > 1 && warningSec == 1)
  {
    messageKey = "alert.time_left.minutes_second";
  }
  else if (warningMin > 1 && warningSec == 0)
  {
    messageKey = "alert.time_left.minutes";
  }
  else if (warningMin == 1 && warningSec > 1)
  {
    messageKey = "alert.time_left.minute_seconds";
  }
  else if (warningMin == 1 && warningSec == 1)
  {
    messageKey = "alert.time_left.minute_second";
  }
  else if (warningMin == 1 && warningSec == 0)
  {
    messageKey = "alert.time_left.minute";
  }
  else if (warningMin == 0 && warningSec > 1)
  {
    messageKey = "alert.time_left.seconds";
  }
  else if (warningMin == 0 && warningSec == 1)
  {
    messageKey = "alert.time_left.second";
  }
  else // if (warningMin == 0 && warningSec == 0)
  {
    messageKey = "alert.time_left.none";
  }

  alert( ASSESSMENT_RESOURCES.getFormattedString( messageKey,
          new Object( {minutes: ''+warningMin, seconds: ''+warningSec} ) ) );
}
