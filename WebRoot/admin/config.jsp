<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/bbNG" prefix="bbNG" %>
<bbNG:learningSystemPage title="JS Test Timer Configuration">
<bbNG:breadcrumbBar environment="COURSE"  includeCourseMenuIcon="true" >
	<bbNG:breadcrumb href="/webapps/portal/execute/tabs/tabAction?tabType=admin">Administrator Panel</bbNG:breadcrumb>
	<bbNG:breadcrumb href="/webapps/portal/admin/pa_ext_caret.jsp">Building Blocks</bbNG:breadcrumb>
	<bbNG:breadcrumb href="/webapps/blackboard/admin/manage_plugins.jsp">Installed Tools</bbNG:breadcrumb>
	<bbNG:breadcrumb>JS Test Timer Configuration</bbNG:breadcrumb>
</bbNG:breadcrumbBar>
<bbNG:pageHeader>
	<bbNG:pageTitleBar title="JS Test Timer Configuration" />
</bbNG:pageHeader>
<p>
This building block has no configurable parameters.
</p>
<p>
See <a href="../">JS Test Timer Help</a> for instructions on using this building block.
</p>
</bbNG:learningSystemPage>