<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="/bbNG" prefix="bbNG" %>
<bbNG:learningSystemPage title="JS Test Timer Help">
<bbNG:breadcrumbBar environment="COURSE"  includeCourseMenuIcon="true" >
	<bbNG:breadcrumb href="/webapps/portal/execute/tabs/tabAction?tabType=admin">Administrator Panel</bbNG:breadcrumb>
	<bbNG:breadcrumb href="/webapps/portal/admin/pa_ext_caret.jsp">Building Blocks</bbNG:breadcrumb>
	<bbNG:breadcrumb href="/webapps/blackboard/admin/manage_plugins.jsp">Installed Tools</bbNG:breadcrumb>
	<bbNG:breadcrumb>JS Test Timer Help</bbNG:breadcrumb>
</bbNG:breadcrumbBar>
<bbNG:pageHeader>
	<bbNG:pageTitleBar title="JS Test Timer Help" />
</bbNG:pageHeader>
<style>
.test-timer p {
	padding-top: 20px;
	font-size: 110%;
}
.test-timer blockquote {
	padding-left: 25px;
}
</style>
<div class="test-timer">
  <p><strong>Forcing Submission of Blackboard Assessments at Time-up</strong></p>
  
  <p>Start by creating a new Test. Give it a Name and Description.</p>
  
  <p>In the "Instructions" section, make sure you select "HTML" below the text box. 
  	Also make sure the Visual Text Box Editor is OFF.
  	Paste the following in the text box:</p>
  	
  <blockquote>
  	<p>
  	 &lt;p&gt;insert your instructions here&lt;/p&gt;<br />
  	 &lt;script language="javascript" src="<%=request.getContextPath()%>/counter_endtime.js" type="text/javascript" &gt;&lt;/script&gt;
  	</p>
  </blockquote>
  
  <p>Type your instructions where it says &quot;insert your instructions here&quot;,
  	between &lt;p&gt; and &lt;/p&gt;.</p>
  	
  <p>Click Submit. Finish creating the test.</p>
  
  <p>Add the Test to your course content. In the &ldquo;Test Options&rdquo; section, under Test Availability, just make sure you set the timer.</p>
  
  <p><strong>The Ten-Second Issue</strong></p>
  
  <p>This script is written to cause the test to submit 10 seconds before 
  the actual time is up, to allow time for the submission process -- though you 
  will encounter some cases where it takes longer than 10 seconds to submit, 
  resulting in a &quot;Needs Grading&quot; message. I would suggest have 
  students use Durham University's Browser Tester building block or something 
  similar, as it has an Alert Test to make sure the 1 minute warning will appear. 
  Sometimes a test will go minutes over the time limit because the student 
  did not see or click away the alert -- but they cannot work on the test 
  during this time either.</p>

  <p><strong>Message to Students</strong></p>

  <p>We suggest you include this message to students in the instructions:</p>
  <blockquote>
     <p><em>If you do not submit the test at least 10 seconds before the time is up, 
     the test will submit automatically. So if the test has a 60 minute time limit, 
     you actually only have 59 minutes, 50 seconds to complete the test, 
     as the time it takes to complete the submission process, 
     which can take up to 10 seconds, is figured in the time.</em>
     </p>
  </blockquote>
  <p>The test will not submit automatically until student clicks away the 1-minute warning pop-up, 
  though the timer keeps ticking -- so if the student waits 2 minutes 
  to click the pop-up, the test will show that the student went 1 minute overtime. 
  However, the student cannot continue working on the test while the pop-up is displayed
   -- nor after it has been clicked away, if the time limit has elapsed.
   </p>
  <p><strong>Separating functionality</strong></p>
	<p>If you prefer to have only the timed test portion or the no-print/no-copy portion of the scripts, they are provided here as separate scripts.</p>
	<p>For timed tests only, follow the instructions above, but use:</p>
	<blockquote>
		<p>
			&lt;p&gt;insert your instructions here&lt;/p&gt;<br />
			&lt;script language="javascript" src="<%=request.getContextPath()%>/counter_endtime-timer-only.js" type="text/javascript" &gt;&lt;/script&gt;
		</p>
	</blockquote>
	<p>For copy and print restrictions use this sequence:</p>
	<blockquote>
		<p>
			&lt;p&gt;insert your instructions here&lt;/p&gt;<br />
			&lt;script language="javascript" src="<%=request.getContextPath()%>/no-copy.js" type="text/javascript" &gt;&lt;/script&gt;
		</p>
	</blockquote>


</div>
</bbNG:learningSystemPage>
  
